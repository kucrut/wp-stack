# WP Stack

## Prerequisites

Make sure you have [Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/) and that you're able to run `docker run hello-world`:

## Usage

If you want to contribute (thank you!):

- Clone this repo and enter the directory:
  ```sh
  git clone https://gitlab.com/kucrut/wp-stack.git wp-stack
  cd wp-stack
  ```
- Setup:
  ```sh
  # TODO
  ```
- Add `127.0.0.1 wp.local` to `/etc/hosts`
- Run the containers:
  ```sh
  docker compose up
  ```
- To bring down the containers:
  ```sh
  docker compose down
  ```
- To run WP CLI command:
  ```sh
  docker compose exec -u www-data backend wp <command>
  ```
- To import database dump:
  ```sh
  docker compose exec -u www-data -T backend wp db import - < PATH_TO_SQL_DUMP_FILE
  ```
- To export database:
  ```sh
  docker compose exec -u www-data -T backend wp db export - > PATH_TO_SQL_DUMP_FILE
  ```
