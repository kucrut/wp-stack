# WP Stack

View README in [English](README-en.md).

## Prasyarat

Pastikan anda telah memasang [Docker](https://docs.docker.com/engine/installation/) dan [Docker Compose](https://docs.docker.com/compose/) dan anda telah berhasil menjalankan `docker run hello-world`:

## Penggunaan

- Clone repo ini lalu masuk ke direktori:
  ```sh
  git clone https://gitlab.com/kucrut/wp-stack.git wp-stack
  cd wp-stack
  ```
- Persiapan:
  ```sh
  # TODO
  ```
- Tambahkan `127.0.0.1 wp.local` ke `/etc/hosts`
- Jalankan container:
  ```sh
  docker compose up
  ```
- Untuk menghentikan container:
  ```sh
  docker compose down
  ```
- Untuk menjalankan WP CLI:
  ```sh
  docker compose exec -u www-data backend wp <command>
  ```
- Untuk mengimpor basis data:
  ```sh
  docker compose exec -u www-data -T backend wp db import - < PATH_TO_SQL_DUMP_FILE
  ```
- Untuk mengekspor basis data:
  ```sh
  docker compose exec -u www-data -T backend wp db export - > PATH_TO_SQL_DUMP_FILE
  ```
